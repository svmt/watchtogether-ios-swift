//
//  LoginViewController.swift
//  Example
//
//  Created by George on 07.12.2020.
//

import UIKit

enum UserDefaultsKey {
    static let userName = "userName"
}

class LoginViewController: UIViewController {
    
    @IBOutlet weak var userNameTextField: UITextField!

    /// Set textfields text to stored data form UserDefaults
    private func setTextFields() {
        guard let userName = UserDefaults.standard.string(forKey: UserDefaultsKey.userName)
        else { return }

        userNameTextField.text = userName

    }


    /// Store text fields data in UserDefaults
    private func storeCredentials() {
        UserDefaults.standard.setValue(userNameTextField.text, forKey: UserDefaultsKey.userName)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        for filed in [userNameTextField] {
            filed?.layer.borderWidth = 0.5
            filed?.layer.borderColor = UIColor.systemBlue.cgColor
            filed?.layer.cornerRadius = 5.0
        }
        setTextFields()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: - Actions
    
    @IBAction func loginAction(_ sender: Any) {
        guard !userNameTextField.text!.isEmpty else {
            print("Please user name")
            let alert = UIAlertController(title: "Mandatory fields", message: "Fields can't be empty!", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            return
        }
        storeCredentials()
        
        // MARK: Use your method to get room token
        let token = "YOUR_TOKEN"
        //let roomName: String = "yourRoomName"
        //token = getToken(for room: roomName)
        assert(token != "YOUR_TOKEN")
        showSession(with: token)
    }
    
    @IBAction func TapAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    // MARK: - Utils
    
    private func showSession(with token: String) {
        DispatchQueue.main.async {
            guard let storyboard = self.storyboard,
                  let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController else {return}
            //vc.roomName = "yourRoomName"
            vc.token = token
            vc.username = self.userNameTextField.text ?? UUID().uuidString
                self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
