//
//  ParticipantSwiftCell.swift
//  Example
//
//  Created by George on 07.12.2020.
//

import UIKit
import WatchTogether
import AVFoundation

class ParticipantSwiftCell: UICollectionViewCell {

    @IBOutlet weak var audioButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var switchCameraButton: UIButton!
    @IBOutlet weak var content: UIView!
    @IBOutlet weak var volumeSlider: UISlider!
    @IBOutlet weak var qualityIndicator: UIImageView!
    @IBOutlet weak var resolutionSlider: UISlider!
    @IBOutlet weak var fpsSlider: UISlider!
    @IBOutlet weak var participantNameLabel: UILabel!
    @IBOutlet weak var visualEfectView: UIVisualEffectView!
    @IBOutlet weak var disconnectButton: UIButton!

    var callback: ((String) -> Void)?
    weak var participant: Participant!{
        didSet {
            guard let participant = participant else {return}

            participant.delegate = self
            participant.speakerDelegate = self
            disconnectButton.isHidden = true
            participantNameLabel.text = participant.getDisplayName()
            participantNameLabel.textColor = .green
            let view = participant.getVideo()
            view.frame = self.bounds
            updateAudioState()
            updateVideoState()
            content.addSubview(view)
            self.bringSubviewToFront(disconnectButton)
        }
    }

    private var controlsHidden: Bool = true {
        didSet {
            audioButton.isHidden = controlsHidden
            videoButton.isHidden = controlsHidden
            volumeSlider.isHidden = controlsHidden

            disconnectButton.isHidden = isLocal ? true : controlsHidden
            switchCameraButton.isHidden = isLocal ? controlsHidden : true
            resolutionSlider.isHidden = isLocal ? controlsHidden : true
            fpsSlider.isHidden = isLocal ? controlsHidden : true
        }
    }

    private var isLocal: Bool {
        get {
            return participant is LocalParticipant
        }
    }

    private var localParticipant: LocalParticipant? {
        get {
            return participant as? LocalParticipant ?? nil
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        self.addGestureRecognizer(tap)
        volumeSlider.value = 10
        resolutionSlider.value = 2
        fpsSlider.value = 30
        controlsHidden = true
    }

    override func prepareForReuse() {
        visualEfectView.isHidden = true
        participant = nil
        for view in self.subviews{
            if view.tag != 1 {
                view.removeFromSuperview()
            }
        }
    }

    // MARK: - Actions

    @IBAction func disconnectTapped(_ sender: UIButton) {
        if let completion = self.callback {
            completion(self.participant.getId())
        }
    }
    @IBAction func muteAudioAction(_ sender: Any) {
        participant.isAudioEnabled = !participant.isAudioEnabled
        updateAudioState()
    }

    @IBAction func muteVideoAction(_ sender: Any) {
        participant.isVideoEnabled = !participant.isVideoEnabled
        updateVideoState()
    }

    @IBAction func onSwitchCamera(_ sender: Any) {
        guard let lParticipant = localParticipant else {
            return
        }

        let newPosition = lParticipant.cameraPosition == .front ? AVCaptureDevice.Position.back
            : AVCaptureDevice.Position.front
        lParticipant.cameraPosition = newPosition
    }

    @IBAction func onVolumeChanged(_ sender: UISlider) {
        participant.volume = sender.value
    }

    @IBAction func onResolutionChanged(_ sender: UISlider) {
        guard let videoQuality = VideoQuality(rawValue: UInt(sender.value)) else {
            return
        }
        localParticipant?.videoQuality = videoQuality
    }

    @IBAction func onFpsChanged(_ sender: UISlider) {
        let fps = Int(sender.value)
        localParticipant?.setFrameRate(fps: fps)
    }

    // MARK: - Gestures

    @objc private func handleTap(_ tap: UITapGestureRecognizer) {
        controlsHidden = !controlsHidden
    }

    // MARK: - Utils

    private func updateAudioState() {
        var audioImage: UIImage? = UIImage()
        if #available(iOS 13.0, *) {
            let audioImageName = participant.isAudioEnabled ? "mic" : "mic.slash"
            audioImage = UIImage(systemName: audioImageName)
        }
        audioButton.setImage(audioImage, for: .normal)
    }

    private func updateVideoState() {
        var videoImage: UIImage? = UIImage()
        if #available(iOS 13.0, *) {
            let videoImageName = participant.isVideoEnabled ? "video" : "video.slash"
            videoImage = UIImage(systemName: videoImageName)
        }
        videoButton.setImage(videoImage, for: .normal)
    }
}

extension ParticipantSwiftCell: ParticipantDelegate{
    func onReconnecting() {
        visualEfectView.isHidden = false
    }

    func onReconnected() {
        visualEfectView.isHidden = true
    }

    func onChangeMediaSettings() {
        updateAudioState()
        updateVideoState()
    }

    func onStreamUpdate(qualityInfo: StreamQualityInfo) {
        switch qualityInfo.quality{
        case .bad:
            qualityIndicator.backgroundColor = .red
            participantNameLabel.textColor = .red
        case .good:
            qualityIndicator.backgroundColor = .yellow
            participantNameLabel.textColor = .yellow
        case .excellent:
            qualityIndicator.backgroundColor = .green
            participantNameLabel.textColor = .green
        @unknown default:
            print("Unknown quality \(qualityInfo.quality)")
        }
    }
}
extension ParticipantSwiftCell: ParticipantActiveSpeakerDelegate {
    func onRemoteParticipantStartSpeaking() {
        DispatchQueue.main.async {
            self.layer.borderWidth = 5
            self.layer.borderColor = UIColor.blue.cgColor
        }
    }
    
    func onRemoteParticipantStopSpeaking() {
        DispatchQueue.main.async {
            self.layer.borderColor = .none
            self.layer.borderWidth = 0
        }
    }
}

class ParticipantView: UIView{
    func setParticipant(_ participant: Participant){
        let viewToAdd = participant.getVideo() // View with video from participant
        let displayName = participant.getDisplayName() // Participant  display name
        let idText = participant.getId()// Participant id
        let titleLabel = UILabel()
        titleLabel.text = displayName + idText
        self.addSubview(titleLabel)
        self.addSubview(viewToAdd)
    }
}
