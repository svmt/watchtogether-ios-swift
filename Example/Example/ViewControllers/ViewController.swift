//
//  ViewController.swift
//  Example
//
//  Created by George Kyrylenko on 12.11.2020.
//

import UIKit
import WatchTogether
import AVFoundation
import CallKit

class ViewController: UIViewController, CXCallObserverDelegate {
    @IBOutlet weak var leaveBtn: UIButton!
    @IBOutlet weak var participantsCollectionView: UICollectionView!
    
    let callObserver = CXCallObserver()
    var token: String!
    var username: String!
    var roomName: String!
    var session: Session?
    var localParticipant: LocalParticipant?
    var localParticipantSnapshot: LocalParticipantSnapshot?
    var participants = [Participant]()
    var isRegularDisconnect = false
    var stoper = false
    var oldVideoState: Bool?
    
    var logsVC: UIViewController!
    var chatVC: ChatViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Session.setMinLogLevel( .debug)
        setupSession()
        setupObservers()
//        callObserver.setDelegate(self, queue: nil)
    }

    // MARK: Initial setup
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        let rightButton = UIBarButtonItem()
        rightButton.target = self
        rightButton.action = #selector(showMenu)
        if #available(iOS 13.0, *) {
            rightButton.image = UIImage(systemName: "ellipsis")
        } else {
            rightButton.title = "..."
        }
        self.navigationItem.rightBarButtonItem = rightButton
        self.navigationItem.title = "Room: \(roomName ?? "Unknown")"
    }

    @objc private func showMenu() {
        let alertView = UIAlertController(title: "Additional functions", message: "", preferredStyle: .actionSheet)
        alertView.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        let logAction = UIAlertAction(title: "View log", style: .default) { action in
            DispatchQueue.main.async { [weak self] in
                self?.present((self?.logsVC)!, animated: true)
            }
        }
        let chatAction = UIAlertAction(title: "Open chat", style: .default) { _ in
            DispatchQueue.main.async { [weak self] in
                self?.openChatView()
            }
        }
        let closeAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        if session?.sessionState == SessionState.connected {
            alertView.addAction(chatAction)
        }
        alertView.addAction(logAction)
        alertView.addAction(closeAction)
        self.present(alertView, animated: true, completion: nil)
    }

    private func openChatView() {
        if self.chatVC == nil {
            guard let storyboard = self.storyboard,
                  let chatVC = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController  else { return }
            self.chatVC = chatVC
        }
        self.chatVC.session = self.session
        self.navigationController?.pushViewController((self.chatVC)!, animated: true)
    }
    
    private func setupObservers() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    // MARK: WatchTogether - SessionFlow
    
    func setupSession() {
        session = SessionBuilder()
            .withUsername(username)
            .withVideoCodec(.H264)
            .withVideoRenderer(.Metal)
            .withDelegate(self)
            .build()
        localParticipant = session?.localParticipant
//        localParticipant?.set(orientation: .landscapeRight)
        // MARK: optional setting -> set to nil to cancel specific orientation.
//        localParticipant?.set(orientation: nil)
        if let snapshot = localParticipantSnapshot {
            localParticipantSnapshot = nil
            localParticipant?.set(snapshot: snapshot)
            if localParticipant?.participantType != .viewer {
                startCameraPreview()
            }
        } else {
            localParticipant?.videoQuality = .Default
        }
        logsVC = session!.createLogsViewController()
    }

    //Scenario A and B
    //A: disconnect on background mode
    //B: shutdown/suspend camera on background mode
    @objc func appMovedToBackground() {
        //Scenario A
//        stoper = true
//        disconnectFlow()
        //Scenario B
        shutdownCameraFlow()
    }
    
    @objc func appDidBecomeActive() {
        //Scenario A
//        self.stoper = false
//        //MARK: to prevent os bug with double call of event
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.010) {
//            if self.stoper == false {
//                self.reconnectFlow()
//            }
//        }
        //Scenario B
        resumeCameraFlow()
    }
    
    func shutdownCameraFlow() {
        oldVideoState = localParticipant?.isVideoEnabled
        localParticipant?.isVideoEnabled = false
    }
    
    func resumeCameraFlow() {
        if let oldVS = oldVideoState {
          localParticipant?.isVideoEnabled = oldVS
        }
    }
    
    func callStarted() {
        disconnectFlow()
    }
        
    func disconnectFlow() {
        if session?.sessionState != .created {
            localParticipantSnapshot = localParticipant?.takeSnapshot()
            isRegularDisconnect = false
            session?.disconnect()
        }
    }
    
    func callEnded() {
        reconnectFlow()
    }
    
    func reconnectFlow() {
        if session == nil {
            setupSession()
            do {
                try session?.connect(with: token)
            } catch {
                print(error)
            }
        }
    }
    
    @IBAction func showLogs(_ sender: Any) {
        self.present(logsVC, animated: true)
    }
    
    // MARK: - Actions
    
    @IBAction func startCameraPreview(_ sender: UIButton) {
        sender.isHidden = true
        startCameraPreview()
    }

    @IBAction func shareButtonTapped(_ sender: Any) {
        print(token ?? "Token not present!")
        let shareUrl = "https://sceenic.watchtogether.test.app/\(token ?? "")"
        UIPasteboard.general.string = shareUrl
        let items = [shareUrl];
        let activity = UIActivityViewController(activityItems: items, applicationActivities: nil);
        activity.popoverPresentationController?.sourceView = sender as! UIButton
        self.present(activity, animated: true, completion: nil)
    }

    func startCameraPreview() {
    #if !targetEnvironment(simulator)
            checkPermitions()
    #else
            enableCamera()
    #endif
    }
    func checkPermitions() {
        AVCaptureDevice.requestAccess(for: .video) { granted in
            print(granted)
        }
        AVCaptureDevice.requestAccess(for: .audio) { granted in
                    self.enableCamera()
        }
    }
    
    func enableCamera() {
        DispatchQueue.main.async {
            guard let localParticipant = self.localParticipant else {return}
            do {
                _ = try localParticipant.startCameraPreview()
            }
            catch {
               print(error)
            }
            self.participants.append(localParticipant)
            self.participantsCollectionView.reloadData()
        }
    }
    @IBAction func changeParticipantType(_ sender: UISegmentedControl) {
        participants.removeAll{$0.getId() == localParticipant?.getId()}
        if sender.selectedSegmentIndex == 0 {
            if session?.sessionState == .connected{
                participants.insert(localParticipant!, at: 0)
            }
            localParticipant?.participantType = .fullParticipant
        } else if sender.selectedSegmentIndex == 1 {
            localParticipant?.participantType = .viewer
        } else {
            if session?.sessionState == .connected {
                participants.insert(localParticipant!, at: 0)
            }
            localParticipant?.participantType = .broadcaster
        }
        
        
        if session?.sessionState == .connected {
            participantsCollectionView.reloadData()
        }
    }
    
    @IBAction func connectToSession(_ sender: UIButton) {
        sender.isHidden = true
        leaveBtn.isHidden = false
        do {
            try self.session?.connect(with: token)
        } catch {
            print(error)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func disconnect(_ sender: Any) {
        isRegularDisconnect = true
        session?.disconnect()
    }
    
    // MARK: You can use callObserver to handle incoming call during session
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
//        if call.hasEnded {
//            callEnded()
//            participantsCollectionView.reloadData()
//        } else {
//            callStarted()
//        }
    }
    
    deinit {
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
}

// MARK: WatchTogether - SessionDelegate

extension ViewController: SessionDelegate {
    func onRemoteParticipantNotification(message: String, participantId: String) {
        let name = participants.filter {$0.getId() == participantId }.first?.getDisplayName()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let stringMsgTime: String = formatter.string(from: Date())
        let adressant = name ?? "user"

        DispatchQueue.main.async { [weak self] in
            if self?.chatVC == nil {
                guard let storyboard = self?.storyboard,
                      let chatVC = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController else { return }
                self?.chatVC = chatVC
            }

            self?.chatVC.messages.append(Message(sender: adressant, message: message, time: stringMsgTime))

            let keyWindow = UIApplication.shared.windows.filter { $0.isKeyWindow }.first

            if let topController = keyWindow?.rootViewController as? UINavigationController {

                if topController.visibleViewController != self?.chatVC {
                    let msgAlert = UIAlertController(title: "Message from \(adressant) at \(stringMsgTime)", message: message, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Open chat", style: .default) { [weak self] (_) in
                        self?.openChatView()
                    }
                    msgAlert.addAction(action)
                    self?.present(msgAlert, animated: true) {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                            msgAlert.dismiss(animated: true, completion: nil)
                        }
                    }
                } else {
                    self?.chatVC.tableView.reloadData()
                    self?.chatVC.scrollToBottom()
                }
            }
        }
    }

    func onRemoteParticipantStopMedia(participant: Participant) {
        self.participants.removeAll { (remove) -> Bool in
            return remove.getId() == participant.getId()
        }
        participantsCollectionView.reloadData()
    }
    
    
    func onSessionError(error: Error) {
        print(error.localizedDescription)
    }
    
    func onSessionConnected(sessionId: String, participants: [Participant]) {
    }
    
    func onSessionDisconnect() {
        self.participants = []
        self.session = nil
        self.localParticipant = nil
        if isRegularDisconnect{
            navigationController?.popViewController(animated: true)
        }
    }
    
    func onRemoteParticipantJoined(participant: Participant) {
    }
    
    func onRemoteParticipantStartMedia(participant: Participant) {
        self.participants.append(participant)
        participantsCollectionView.reloadData()
    }
    
    func onRemoteParticipantLeft(participant: Participant) {
        self.participants.removeAll { (participantForRemove) -> Bool in
            return participantForRemove.getId() == participant.getId()
        }
        participantsCollectionView.reloadData()
    }
    func onDisconnected(reason: String?, initiator: String) {
        print(reason)
    }
}

    // MARK: CollectionViewDelegates

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return participants.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParticipantSwiftCell", for: indexPath) as? ParticipantSwiftCell else {
            return UICollectionViewCell()
        }
        cell.participant = participants[indexPath.row]
        return cell
    }
}

    // MARK: CollectionViewDelegateFlowLayout

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (UIScreen.main.bounds.size.width / 2) - 2.0
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
                            collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}
