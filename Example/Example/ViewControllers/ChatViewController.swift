//
//  ChatViewController.swift
//  Example
//
//  Created by Anton Umnitsyn on 30.07.2021.
//

import UIKit
import WatchTogether

struct Message {
    var sender: String!
    var message: String!
    var time: String!

    init(sender: String!, message: String!, time: String!) {
        self.sender = sender
        self.message = message
        self.time = time
    }
}

class ChatViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var messageTextField: UITextField!
    @IBOutlet var sendMessageButton: UIButton!
    @IBOutlet var inputContainer: UIView!
    @IBOutlet var inputContainerBottomConstrain: NSLayoutConstraint!

    var session: Session!
    var messages: [Message] = []
    var keyboardOffset: CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) { [weak self] notification in
            let key = UIResponder.keyboardFrameEndUserInfoKey
            guard let value = notification.userInfo?[key] as? NSValue else {
                return
            }
            let keyboardHeight = value.cgRectValue.height - self!.keyboardOffset
            self?.inputContainerBottomConstrain.constant = keyboardHeight + 10.0
            self?.scrollToBottom()
        }

        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) { [weak self] (_) in
            self?.inputContainerBottomConstrain.constant = 10.0
            self?.scrollToBottom()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        scrollToBottom()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func tapAction(_ sender: Any) {
        view.endEditing(true)
    }

    @IBAction func sendMessageTapped(_ sender: Any) {
        guard let message = messageTextField.text else { return }
        guard let session = self.session, !message.isEmpty else { return }
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let stringMsgTime: String = formatter.string(from: Date())
        messages.append(Message(sender: "me", message: messageTextField.text!, time: stringMsgTime))
        session.sendMessage(message: messageTextField.text!)
        messageTextField.text = ""
        tableView.reloadData()
        scrollToBottom()
    }

    func scrollToBottom() {
        if self.messages.count > 0 {
            DispatchQueue.main.async { [weak self] in

                let indexPath = IndexPath(row: (self?.messages.count)! - 1, section: 0)
                self?.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
}

extension ChatViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]

        let identifier = message.sender == "me" ? "ChatCell2" : "ChatCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ChatTableViewCell
        cell.messageLabel?.text = message.message
        cell.timeLabel?.text = message.time
        if message.sender != "me" {
            cell.nameLabel?.text = message.sender
        }
        return cell
    }
}
