# Sceenic WatchTogether iOS SDK
Sceenic's Watch Together video-chat is a general-purpose video chatting infrastructure that is aimed to provide high-quality Audio and Video functionality that will allow you to create and provide engaging experiences in your application for your customers


## Documentation
Have a look at our official documentation site [documentation page](https://documentation.sceenic.co/sscale-confluence-tutorials/sscale-confluence-ios/ios-swift-objective-c-adapter-new)

## Requirements
- Xcode
- Watch Together SDK, API_KEY, and API_SECRET - can be retrieved in your private area once you login - [Go to the private area](https://media.sceenic.co/)
- A working authentication server - [Authentication overview](https://documentation.sceenic.co/sscale-confluence-overview)

In case you don't have access, but want to use our service please contact us through the website https://www.sceenic.co/

## Installation

To get our sample working, go to our iOS tutorials and follow the instructions 
- [Swift & Objective-c adapter](https://documentation.sceenic.co/sscale-confluence-tutorials/sscale-confluence-ios/ios-swift-objective-c-adapter-new)
